package com.victorsand.doittogetherandroid.dummy;

import com.victorsand.doittogetherandroid.Task;
import com.victorsand.doittogetherandroid.TaskList;

/**
 * Created by victor on 2015-10-17.
 */
public class DummyTaskLists
{

   private TaskList mOther;
   private TaskList mHome;
   private TaskList mGroceries;

   public DummyTaskLists()
   {
      mHome = new TaskList("Home");
      mHome.addTask(new Task("Clean the bathroom", mHome));
      mHome.addTask(new Task("Fix the roof", mHome));
      mHome.addTask(new Task("Buy more power tools", mHome));

      mGroceries = new TaskList("Groceries");
      mGroceries.addTask(new Task("Milk", mGroceries));
      mGroceries.addTask(new Task("Butter", mGroceries));
      mGroceries.addTask(new Task("Eggs", mGroceries));
      mGroceries.addTask(new Task("Fun drugs", mGroceries));

      mOther = new TaskList("Other");
      mOther.addTask(new Task("Reservations at Dorsia", mOther));
      mOther.addTask(new Task("Get rich", mOther));
   }
   
   public TaskList Home()
   {
      return mHome;
   }
   
   public TaskList Groceries()
   {
      return mGroceries;
   }
   
   public TaskList Other()
   {
      return mOther;
   }
   
   

}
