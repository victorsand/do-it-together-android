package com.victorsand.doittogetherandroid;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.net.URL;

/**
 * Created by victor on 2015-10-23.
 */
public class PostTask extends AsyncTask<String, String, String>
{
    private static String TAG = "PostTask";

    private URL mUrl;
    private TaskCallback mTaskCallback;
    private ProgressDialog mProgressDialog;
    private Task mTask;

    public PostTask(URL url, Task task, ProgressDialog progressDialog, TaskCallback taskCallback)
    {
        mUrl = url;
        mTaskCallback = taskCallback;
        mProgressDialog = progressDialog;
        mTask = task;
    }

    @Override
    protected String doInBackground(String... params)
    {
        return null;
    }
}
