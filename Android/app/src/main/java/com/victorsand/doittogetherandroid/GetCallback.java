package com.victorsand.doittogetherandroid;

import android.util.Log;

/**
 * Created by victor on 2015-10-23.
 */
public class GetCallback implements IGetCallback
{
    private static String TAG = "GetCallback";

    @Override
    public void onDataReceived(String data)
    {
        Log.d(TAG, "onDataReceived");
        Log.d(TAG, data);

    }
}
