package com.victorsand.doittogetherandroid;

import android.app.ProgressDialog;
import android.util.Log;
import java.net.URL;

public class ServerAPI implements IServerAPI
{

    private static String TAG = "ServerAPI";

    @Override
    public void getTaskLists(URL url, ProgressDialog progressDialog, final IGetCallback getCallback)
    {
        Log.d(TAG, "getTaskLists");
        new GetTask(url, progressDialog, new TaskCallback()
        {
            @Override
            public void onTaskComplete(String result)
            {
                Log.d("GetTask", "onTaskComplete");
                getCallback.onDataReceived(result);
            }
        }).execute();
    }

    @Override
    public void postTask(URL url, Task task, ProgressDialog progressDialog, final IPostCallback postCallback)
    {
        Log.d(TAG, "postTask");
        new PostTask(url, task, progressDialog, new TaskCallback()
        {
            @Override
            public void onTaskComplete(String result)
            {
                Log.d("PostTask", "onTaskComplete");
                postCallback.onDataReceived(result);
            }
        }).execute();
    }
}
