package com.victorsand.doittogetherandroid;

import android.util.Log;

import com.victorsand.doittogetherandroid.Task;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 2015-10-17.
 */
public class TaskList
{
    private static String TAG = "TaskList";

    private String mTitle;
    private List<Task> mTasks;
    private TaskListId mTaskListId;

    public TaskList(String title)
    {
        mTitle = title;
        mTasks = new ArrayList<>();
        mTaskListId = new TaskListId();
        Log.d(TAG, "Constructed task list with ID " + mTaskListId.toInt());
    }


    public void addTask(Task task)
    {
        if (task.getParentTaskList() != this)
        {
            throw new IllegalArgumentException("Tasks can only be added to their parent task lists");
        }
        mTasks.add(task);
    }

    public void setTitle(String title)
    {
        mTitle = title;
    }

    public Task getTask(int position)
    {
        return mTasks.get(position);
    }

    public String getTitle()
    {
        return mTitle;
    }

    public String getTitleWithSize()
    {
        return mTitle + " (" + getSize() + ")";
    }

    public int getSize()
    {
        return mTasks.size();
    }

    public List<Task> getTasks()
    {
        return mTasks;
    }

    public TaskListId getTaskListId()
    {
        return mTaskListId;
    }

}
