package com.victorsand.doittogetherandroid;

import android.app.ProgressDialog;
import android.graphics.Paint;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import org.json.JSONException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements TaskListFragment.OnFragmentInteractionListener {

    private static String TAG = "MainActivity";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private Map<Integer, TaskListFragment> mTaskListFragments;
    private Map<Integer, TaskList> mTaskLists;

    private IServerAPI mServerAPI;
    private IJSONParser mJSONParser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        mTaskListFragments = new HashMap<>();
        mTaskLists = new HashMap<>();

        mServerAPI = new ServerAPI();
        mJSONParser = new JSONParser();

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(mViewPager);

        updateTaskLists();
    }

    private void updateTaskLists()
    {
        URL url;
        try
        {
            url = new URL("http://10.0.2.2:8080/lists/testlists");
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return;
        }

        mServerAPI.getTaskLists(url, new ProgressDialog(MainActivity.this), new GetCallback()
        {
            @Override
            public void onDataReceived(String data)
            {
                Log.d(TAG, "onDataReceived");
                try
                {
                    ArrayList<TaskList> taskLists = mJSONParser.getTaskListsFromJSONString(data);
                    for (TaskList i : taskLists)
                    {
                        TaskList taskList = i;
                        mTaskLists.put(taskList.getTaskListId().toInt(), taskList);
                        TaskListFragment taskListFragment = new TaskListFragment();
                        taskListFragment.setTaskList(taskList);
                        mTaskListFragments.put(taskList.getTaskListId().toInt(), taskListFragment);
                    }

                    mSectionsPagerAdapter.notifyDataSetChanged();
                    TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(mViewPager);
                }
                catch (JSONException e)
                {
                    Log.e(TAG, "Failed to parse server response");
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onTaskListClick(Task task)
    {
        Log.d(TAG, "clicked " + task.getItemTitle());
    }

    @Override
    public void onTaskCheckBoxClick(Task task, TextView textView)
    {
        Log.d(TAG, "clicked checkbox for " + task.getItemTitle());
        if (!task.GetDone())
        {
            Log.d(TAG, "done!");
            task.SetDone(true);
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else
        {
            Log.d(TAG, "undone!");
            task.SetDone(false);
            textView.setPaintFlags(textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            Log.d(TAG, "SectionsPagerAdapter constructor complete");
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "getItem(" + position + ")");
            if (!mTaskListFragments.containsKey(position))
            {
                throw new IllegalArgumentException("Trying to get a non-existant fragment");
            }
            return mTaskListFragments.get(position);
        }

        @Override
        public int getCount() {
            return mTaskLists.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (!mTaskLists.containsKey(position))
            {
                throw new IllegalArgumentException("Trying to get a non-existant task list");
            }
            return mTaskLists.get(position).getTitleWithSize();
        }
    }

}
