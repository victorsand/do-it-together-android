package com.victorsand.doittogetherandroid;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class JSONParser implements IJSONParser
{

    private static String TAG = "JSONParser";

    public JSONParser()
    {
    }

    @Override
    public ArrayList<TaskList> getTaskListsFromJSONString(String json) throws JSONException
    {
        Log.d(TAG, "getTaskListsFromJSONString");
        JSONObject root = new JSONObject(json);
        ArrayList<TaskList> taskLists = new ArrayList<>();
        JSONArray lists = root.getJSONArray("lists");
        for (int i=0; i<lists.length(); ++i)
        {
            TaskList taskList = getTaskListFromJSON(lists.getJSONObject(i));
            taskLists.add(taskList);
        }
        return taskLists;
    }

    private TaskList getTaskListFromJSON(JSONObject json) throws JSONException
    {
        Log.d(TAG, "getTaskListFromJSON");
        String name = json.getString("name");
        TaskList taskList = new TaskList(name);
        JSONArray tasksJson = json.getJSONArray("tasks");
        for (int i=0; i<tasksJson.length(); ++i)
        {
            Task task = new Task(tasksJson.getJSONObject(i).getString("body"), taskList);
            taskList.addTask(task);
        }
        return taskList;
    }

}
