package com.victorsand.doittogetherandroid;

/**
 * Created by victor on 2015-10-15.
 */

public class Task
{
    private String mItemTitle;
    private TaskList mParentTaskList;

    private boolean mDone;

    public Task(String title, TaskList parentTaskList)
    {
        this.mItemTitle = title;
        this.mParentTaskList = parentTaskList;
        this.mDone = false;
    }

    public String getItemTitle()
    {
        return mItemTitle;
    }

    public void setItemTitle(String mItemTitle)
    {
        this.mItemTitle = mItemTitle;
    }

    public TaskList getParentTaskList()
    {
        return mParentTaskList;
    }

    public boolean GetDone()
    {
        return mDone;
    }

    public void SetDone(boolean done)
    {
        mDone = done;
    }


}
