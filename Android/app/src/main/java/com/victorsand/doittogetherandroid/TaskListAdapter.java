package com.victorsand.doittogetherandroid;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * Created by victor on 2015-10-15.
 */
public class TaskListAdapter extends ArrayAdapter
{
    private Context context;
    private static String TAG = "TaskListAdapter";
    private TaskListFragment mParentFragment;

    public TaskListAdapter(Context context, List items, TaskListFragment parentFragment)
    {
        super(context, android.R.layout.simple_list_item_1, items);
        this.context = context;
        this.mParentFragment = parentFragment;
        Log.d(TAG, "constructor complete");
    }

    private class ViewHolder
    {
        TextView taskTitle;
        CheckBox taskCheckBox;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Log.d(TAG, "getView("+position+")");

        final ViewHolder viewHolder;
        final Task item = (Task) getItem(position);
        View viewToUse;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
        {
            viewToUse = inflater.inflate(R.layout.task_item, null);
            viewHolder = new ViewHolder();
            viewHolder.taskTitle = (TextView) viewToUse.findViewById(R.id.taskTextView);
            viewHolder.taskCheckBox = (CheckBox) viewToUse.findViewById(R.id.taskCheckBox);
            viewToUse.setTag(viewHolder);
        }
        else
        {
            viewToUse = convertView;
            viewHolder = (ViewHolder) viewToUse.getTag();
        }

        viewHolder.taskTitle.setText(item.getItemTitle());
        viewHolder.taskCheckBox.setChecked(item.GetDone());

        viewHolder.taskCheckBox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    OnTaskCheckBoxClickListener listener = mParentFragment;
                    listener.onTaskCheckBoxClick(item, viewHolder.taskTitle);
                }
                catch (ClassCastException e)
                {
                    throw new ClassCastException(mParentFragment.toString() +
                            " must implement OnTaskCheckBoxClickListener interface");
                }
            }
        });

        return viewToUse;
    }

    public interface OnTaskCheckBoxClickListener
    {
        void onTaskCheckBoxClick(Task task, TextView textView);
    }

}
