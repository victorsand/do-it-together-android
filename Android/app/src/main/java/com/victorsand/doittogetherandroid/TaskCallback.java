package com.victorsand.doittogetherandroid;

/**
 * Created by victor on 2015-10-23.
 */
public abstract class TaskCallback
{
    public abstract void onTaskComplete(String result);
}
