package com.victorsand.doittogetherandroid;

import android.app.ProgressDialog;

import java.net.URL;

/**
 * Created by victor on 2015-10-23.
 */
public interface IServerAPI
{
    void getTaskLists(URL url, ProgressDialog progressDialog, final IGetCallback getCallback);
    void postTask(URL url, Task task, ProgressDialog progressDialog, final IPostCallback postCallback);
}
