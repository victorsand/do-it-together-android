package com.victorsand.doittogetherandroid;

public interface IGetCallback
{
    void onDataReceived(String data);
}
