package com.victorsand.doittogetherandroid;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class GetTask extends AsyncTask<String, String, String>
{

    private static String TAG = "GetTask";

    private URL mUrl;
    private TaskCallback mTaskCallback;
    private ProgressDialog mProgressDialog;

    public GetTask(URL url, ProgressDialog progressDialog, TaskCallback taskCallback)
    {
        mUrl = url;
        mTaskCallback = taskCallback;
        mProgressDialog = progressDialog;
    }

    @Override
    protected void onPreExecute()
    {
        Log.d(TAG, "onPreExecute");
        mProgressDialog.setMessage("Loading data...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    @Override
    protected String doInBackground(String... params)
    {
        Log.d(TAG, "doInBackground");
        HttpURLConnection httpURLConnection;
        try
        {
            httpURLConnection = (HttpURLConnection) mUrl.openConnection();
        }
        catch (IOException e)
        {
            Log.e(TAG, "Failed to open connection");
            e.printStackTrace();
            return null;
        }

        StringBuilder sb = new StringBuilder();
        try
        {
            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                sb.append(line);
            }
        }
        catch (IOException e)
        {
            Log.e(TAG, "Failed to read input stream");
            e.printStackTrace();
            return null;
        }
        finally
        {
            httpURLConnection.disconnect();
        }

        return sb.toString();
    }

    @Override
    protected void onPostExecute(String data)
    {
        Log.d(TAG, "onPostExecute");
        mProgressDialog.dismiss();
        mTaskCallback.onTaskComplete(data);
    }

}
