package com.victorsand.doittogetherandroid;

import org.json.JSONException;

import java.util.ArrayList;

public interface IJSONParser
{
    ArrayList<TaskList> getTaskListsFromJSONString(String json) throws JSONException;
}
