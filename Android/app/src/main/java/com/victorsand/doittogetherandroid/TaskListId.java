package com.victorsand.doittogetherandroid;

/**
 * Created by victor on 2015-10-17.
 */
public class TaskListId
{

    private static int globalId = 0;
    private int mId;

    public TaskListId()
    {
        mId = globalId;
        globalId++;
    }

    public int toInt()
    {
        return mId;
    }
}
