package com.victorsand.doittogetherandroid;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;


/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class TaskListFragment extends ListFragment implements TaskListAdapter.OnTaskCheckBoxClickListener
{
    private static String TAG = "TaskListFragment";

    private OnFragmentInteractionListener mListener;
    private TaskList mTaskList;
    private TaskListAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TaskListFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        if (mTaskList == null)
        {
            Log.e(TAG, "Task list has not been set");
            return;
        }
        mAdapter = new TaskListAdapter(getActivity(), mTaskList.getTasks(), this);
        setListAdapter(mAdapter);
    }

    public void setTaskList(TaskList taskList)
    {
        mTaskList = taskList;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        Log.d(TAG, "onDetach");
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);
        Log.d(TAG, "onListItemClick");
        mListener.onTaskListClick(mTaskList.getTask(position));
    }

    @Override
    public void onTaskCheckBoxClick(Task task, TextView textView)
    {
        Log.d(TAG, "onTaskCheckBoxCLick");
        mListener.onTaskCheckBoxClick(task, textView);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        void onTaskListClick(Task task);
        void onTaskCheckBoxClick(Task task, TextView textView);
    }

}
