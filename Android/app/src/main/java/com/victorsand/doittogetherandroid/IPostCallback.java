package com.victorsand.doittogetherandroid;

/**
 * Created by victor on 2015-10-23.
 */
public interface IPostCallback
{
    void onDataReceived(String data);
}
